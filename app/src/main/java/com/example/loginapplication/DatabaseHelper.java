package com.example.loginapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "login_application.db";
    public static final String USERS_TABLE_NAME = "users_table";
    public static final String MESSAGES_TABLE_NAME = "messages";
    public static final String USERS_COL_1 = "ID";
    public static final String USERS_COL_2 = "Name";
    public static final String USERS_COL_3 = "Password";
    public static final String MESSAGES_COL_1 = "ID";
    public static final String MESSAGES_COL_2 = "from";
    public static final String MESSAGES_COL_3 = "message";

    /**
     * Create a helper object to create, open, and/or manage a database.
     * This method always returns very quickly.  The database is not actually
     * created or opened until one of {@link #getWritableDatabase} or
     * {@link #getReadableDatabase} is called.
     *
     * @param context to use for locating paths to the the database
     */
    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format(
            "create table %s (%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT);",
            USERS_TABLE_NAME, USERS_COL_1, USERS_COL_2, USERS_COL_3
        );

        String query2 = String.format(
                "create table %s (%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT);",
                "messages", "ID", "From_user", "message"
        );

        db.execSQL(query);
        db.execSQL(query2);
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     *
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = String.format("DROP TABLE IF EXISTS %s", USERS_TABLE_NAME);
        String query2 = String.format("DROP TABLE IF EXISTS %s", MESSAGES_TABLE_NAME);

        db.execSQL(query);
        db.execSQL(query2);
        onCreate(db);
    }

    public boolean registerUser(String name, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(USERS_COL_2, name);
        contentValues.put(USERS_COL_3, password);

        long result = db.insert(USERS_TABLE_NAME, null, contentValues);

        return result != -1;
    }

    public Boolean loginUser(String name, String password) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = String.format(
            "select * from %s where %s = ? and %s = ?",
            USERS_TABLE_NAME, USERS_COL_2, USERS_COL_3
        );

        Cursor result = db.rawQuery(query, new String[] {name, password});

        return result.getCount() > 0;
    }

    public boolean addMessage(String from, String message) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("From_user", from);
        contentValues.put("message", message);

        long result = db.insert("messages", null, contentValues);

        return result != -1;
    }

    public Cursor getAllMessages() {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = String.format("select * from %s", "messages");

        return db.rawQuery(query, null);
    }
}
