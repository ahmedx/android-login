package com.example.loginapplication;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Register extends AppCompatActivity {
    EditText name, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name = findViewById(R.id.name);
        password = findViewById(R.id.password);

        RegisterUser();
    }

    public void RegisterUser() {
        final DatabaseHelper db = new DatabaseHelper(this);

        Button btnRegister = findViewById(R.id.button_register);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("") || password.getText().toString().equals(""))
                    return;

                boolean isRegistered = db.registerUser(name.getText().toString(), password.getText().toString());

                if (isRegistered)
                    Toast.makeText(Register.this, "User registered", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(Register.this, "User not registered", Toast.LENGTH_LONG).show();

                name.setText("");
                password.setText("");
            }
        });
    }
}
