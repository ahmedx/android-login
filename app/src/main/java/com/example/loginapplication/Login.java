package com.example.loginapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class Login extends AppCompatActivity {
    EditText name, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name = findViewById(R.id.name);
        password = findViewById(R.id.password);

        LoginUser();
    }

    public void LoginUser() {
        // check if name and password match -> redirect to chat
        Button btnLogin = findViewById(R.id.button_login);
        final DatabaseHelper db = new DatabaseHelper(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("") || password.getText().toString().equals(""))
                    return;

                Boolean isLoggedIn = db.loginUser(name.getText().toString(), password.getText().toString());

                if (isLoggedIn) {
                    Toast.makeText(Login.this, "User logged in", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Login.this, Main2Activity.class);
                    i.putExtra("EXTRA_SESSION_ID", name.getText().toString());
                    startActivity(i);
                }
                else
                    Toast.makeText(Login.this, "Invalid credentials", Toast.LENGTH_LONG).show();

                name.setText("");
                password.setText("");
            }
        });
    }
}
