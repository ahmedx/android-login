package com.example.loginapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {
    EditText message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        message = findViewById(R.id.editTextMessage);

        Button btnSendMessage = findViewById(R.id.button_send);
        Button btnFetchMessages = findViewById(R.id.button_fetch_message);

        final DatabaseHelper db = new DatabaseHelper(this);

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sessionId = getIntent().getStringExtra("EXTRA_SESSION_ID");

                if (message.getText().toString().equals(""))
                    return;

                boolean isMessageAdded = db.addMessage(sessionId, message.getText().toString());

                if (isMessageAdded)
                    Toast.makeText(Main2Activity.this, "Message Sent", Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(Main2Activity.this, "Message not sent", Toast.LENGTH_LONG).show();

                message.setText("");
            }
        });

        btnFetchMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Cursor result = db.getAllMessages();
                if (result.getCount() == 0) {
                    ShowMessage("Error", "No messages found");
                    return;
                }

                StringBuffer buffer = new StringBuffer();

                while (result.moveToNext()) {
                    buffer.append(String.format("From: %s \n", result.getString(1)));
                    buffer.append(String.format("Message: %s \n\n", result.getString(2)));
                }

                // show all data
                ShowMessage("Data", buffer.toString());
            }
        });
    }

    public void ShowMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}
